FROM python:3.13.0a4-slim-bullseye
WORKDIR /app
COPY main.py /app
ENTRYPOINT [ "python3", "main.py" ]