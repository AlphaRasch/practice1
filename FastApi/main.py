from fastapi import FastAPI
import random
import redis

app = FastAPI()
redis_db = redis.StrictRedis(host='redis', port=6379, db=0)

def generate_sorted_array(size):
    array = [random.randint(1, 1000) for _ in range(size)]
    array.sort()
    redis_db.set('sorted_array', ','.join(map(str, array)))
    return array

def binary_search(array, target):
    left, right = 0, len(array) - 1
    while left <= right:
        mid = (left + right) // 2
        if array[mid] == target:
            return mid
        elif array[mid] < target:
            left = mid + 1
        else:
            right = mid - 1
    return -1

@app.get("/search/{number}")
async def search_number(number: int):
    array = redis_db.get('sorted_array')
    if array:
        array = list(map(int, array.decode().split(',')))
    else:
        array = generate_sorted_array(100)

    index = binary_search(array, number)
    if index != -1:
        return {"index": index}
    else:
        return {"index": -1}
