import random
import sys

def binary_search(array, target):
    left = 0
    right = len(array) - 1

    while left <= right:
        mid = (left + right) // 2

        if array[mid] == target:
            return mid
        elif array[mid] < target:
            left = mid + 1
        else:
            right = mid - 1

    return -1

if __name__ == '__main__':
    # Генерация случайного отсортированного массива
    array = sorted([random.randint(1, 100) for _ in range(100)])
    #print(array)
    # Получение искомого значения из параметров командной строки
    #target = int(input("Введите искомое значение: "))
    #print(sys.argv)
    if len(sys.argv) > 1:
        target = int(sys.argv[1])
    else:
        target = int(input("Введите искомое значение: "))
    # Поиск значения в массиве с помощью бинарного алгоритма
    result = binary_search(array, target)

    if result != -1:
        print(f"Значение {target} найдено в массиве на позиции {result}.")
    else:
        print(f"Значение {target} не найдено в массиве.")
